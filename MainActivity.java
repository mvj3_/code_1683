package com.example.sina_dimen;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.weibo.sdk.android.Oauth2AccessToken;
import com.weibo.sdk.android.Weibo;
import com.weibo.sdk.android.WeiboAuthListener;
import com.weibo.sdk.android.WeiboDialogError;
import com.weibo.sdk.android.WeiboException;

public class MainActivity extends Activity {
	private Button authBtn;
	private Weibo mWeibo;
	private static final String appKey = "322414668";
	private static final String redirectUrl = "http://blog.163.com/yangpumengchao";
	public static Oauth2AccessToken accessToken;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		authBtn = (Button) findViewById(R.id.btn_auth);
		mWeibo = Weibo.getInstance(appKey, redirectUrl);
		authBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				mWeibo.authorize(MainActivity.this, new authDialogListener());
			}
		});
	}
	
	class authDialogListener implements WeiboAuthListener{

		@Override
		public void onCancel() {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onComplete(Bundle bundle) {
			String token = bundle.getString("access_token");
            String expires_in = bundle.getString("expires_in");
            MainActivity.accessToken = new Oauth2AccessToken(token, expires_in);
		}

		@Override
		public void onError(WeiboDialogError arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onWeiboException(WeiboException arg0) {
			// TODO Auto-generated method stub
			
		}
		
	}

}
